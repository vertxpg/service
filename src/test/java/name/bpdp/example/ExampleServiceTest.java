package name.bpdp.example;

import io.vertx.codetrans.annotations.CodeTranslate;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpServer;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestOptions;
import io.vertx.ext.unit.TestSuite;
import io.vertx.ext.unit.report.ReportOptions;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.core.DeploymentOptions;

public class ExampleServiceTest {

	Vertx vertx = Vertx.vertx();

	public static void main(String[] args) {
		new ExampleServiceTest().run();
	}

	public void run() {

		TestOptions options = new TestOptions().addReporter(new ReportOptions().setTo("console"));
		TestSuite suite = TestSuite.create("Test suite for Example-service");
		suite.test("initialize", context -> {

			JsonObject config = new JsonObject().put("address", "mycomputer.mynetwork");
			DeploymentOptions depOptions = new DeploymentOptions().setConfig(config);

			vertx.deployVerticle("service:name.bpdp.example-service", depOptions, res -> {
				if (res.succeeded()) {
					System.out.println("Start service - succeed");
				} else {
					System.out.println("Start service - failed");
				}
			});

			String s = "value";
			context.assertEquals("value", s);
		});

		suite.run(options);

	}

}
